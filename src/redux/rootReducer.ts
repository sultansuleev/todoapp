import {combineReducers} from 'redux'

import {newTodoReducer} from './reducers/newTodo'
import {todoReducer} from './reducers/todos'

export const rootReducer = combineReducers({
    newTodo: newTodoReducer, todos: todoReducer
})

export type RootState = ReturnType<typeof rootReducer>