interface State {
    newTodo: string
}

const initialState: State = {
    newTodo: ""
}

export const newTodoReducer = (state = initialState, action: any) => {
    switch (action.type) {
        case 'UpdNewTodo':
            return {
                newTodo: action.payload
            }
        default:
            return state
    }
}