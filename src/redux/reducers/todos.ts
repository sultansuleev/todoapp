interface State {
    todos: Array<Todo>
}

const initialState: State = {
    todos: []
}

export const todoReducer = (state = initialState, action: any) => {
    switch (action.type) {
        case 'AddTodo':
            return {
                todos: [...state.todos, action.payload]
            }
        case 'UpdTodos':
            return {
                todos: action.payload
            }
        default:
            return state
    }
}