import React from 'react'
import {Layout} from 'antd'
import 'antd/dist/antd.css';
import {TodoList} from './component/TodoList'
import {TodoForm} from './component/TodoForm'
import styled from "@emotion/styled"
import {useSelector, useDispatch} from "react-redux"
import { RootState } from './redux/rootReducer';

const {Header, Content} = Layout;

function App() {
    const todos = useSelector((state: RootState ) => state.todos.todos)
    const dispatch = useDispatch()

    const toggleComplete: ToggleComplete = selectedTodo => {
        const updatedTodos = todos.map((todo: Todo) => {
            if (todo === selectedTodo) {
                return {...todo, complete: !todo.complete}
            }
            return todo
        })
        dispatch({type: 'UpdTodos', payload: updatedTodos})
    }

    const MainLayout = styled.div`
      display: flex;
      flex-direction: column;
      justify-content: center;
      align-items: center;
      text-align: center;
      padding-top: 2em;
    `

    return (
        <MainLayout>
            <Header style={{backgroundColor: '#fff'}}>
                <h1> Todo App</h1>
            </Header>
            <Content>
                <TodoForm />
                {todos.length > 0 &&
                    <TodoList todos={todos} toggleComplete={(selectedTodo: any) => toggleComplete(selectedTodo)}/>
                }

            </Content>
        </MainLayout>
    );
}

export default App;
