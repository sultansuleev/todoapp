import React, { FC, FormEvent} from "react"

import { Form, Input, Button } from 'antd';
import {useDispatch, useSelector} from "react-redux";
import {RootState} from "../redux/rootReducer";

export const TodoForm: FC = (props) => {
    const newTodo = useSelector((state: RootState) => state.newTodo.newTodo);
    const dispatch = useDispatch()

    const handleChange = (e: string) => {
        dispatch({type: 'UpdNewTodo', payload: e})
    }

    const handleSubmit = (e: FormEvent<HTMLButtonElement>) => {
        if (newTodo !== "")
            dispatch({type: 'AddTodo', payload:{text: newTodo, complete: false}})

        dispatch({type: 'UpdNewTodo', payload: ""})
    }

    return (
        <Form
            name="basic"
            onFinish={handleSubmit}
            autoComplete="off"
            style={{
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'center'
            }}
        >
            <Form.Item
                name="todo"
            >
                <Input placeholder='Add a todo' onChange={(e) => handleChange(e.target.value)}/>
            </Form.Item>

            <Form.Item
            >
                <Button type="primary" htmlType={"submit"} >
                    Submit
                </Button>

            </Form.Item>
        </Form>
    )
}