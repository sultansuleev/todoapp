import React, {FC} from "react"
import styled from "@emotion/styled"
import {Checkbox} from 'antd'

interface Props {
    todo: Todo
    toggleComplete: ToggleComplete
}

interface LabelProps {
    complete: boolean
}

const Label = styled.label<LabelProps>`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  box-shadow: rgba(149, 157, 165, 0.2) 0px 8px 24px;
  margin: 1em auto;

  padding: 16px;
  border-radius: 20px;
  width: max-content;

  text-decoration: ${(props) => props.complete && 'line-through'};
  opacity: ${(props) => props.complete && '0.4'};

  & input {
    margin-right: 10px;
  }
`

export const TodoListItem: FC<Props> = ({todo, toggleComplete}) => {
    return (
        <li>
            <Label complete={todo.complete}>
                <Checkbox onChange={() => toggleComplete(todo)} checked={todo.complete}>
                    {todo.text}
                </Checkbox>
            </Label>
        </li>
    )
}