import React, {FC} from "react"
import {TodoListItem} from "./TodoListItem"
import styled from "@emotion/styled";

interface Props {
    todos: Array<Todo>
    toggleComplete: ToggleComplete
}

export const TodoList: FC<Props> = ({ todos, toggleComplete }) => {
    const Ul = styled.ul`
      list-style-type:none;
      padding: 0;
    `

    return (
        <Ul>
            {todos.map(todo => (
                <TodoListItem
                    key={todo.text}
                    todo={todo}
                    toggleComplete={toggleComplete}
                />
            ))}
        </Ul>
    )
}